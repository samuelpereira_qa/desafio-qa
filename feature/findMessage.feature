Funcionalidade: Encontrar mensagem

Como um usuário esquecido
Desejo digitar uma ou mais palavras chaves
Para que eu possa conseguir encontrar a mensagem desejada


Cenário: Encontrar mensagem - Com Sucesso

Dado que é desejado encontrar as mensagens com a palavra chave qualidade 
Quando é digitado
Então a pesquisa é realizada com sucesso
E as mensagens são exibidas como resultado


Cenário: Encontrar mensagem - Sem Sucesso

Dado que é desejado encontrar as mensagens com a palavra chave testes manuais
Quando é digitado
Então a pesquisa é realizada
E o usuário é informado que não houve resultados
