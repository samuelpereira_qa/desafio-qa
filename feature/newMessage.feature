Funcionalidade: Nova mensagem instantanea

Como um bom amigo
Eu gostaria de enviar uma mensagem para um amigo
Para que possamos combinar algo para o fim de semana

Cenário: Mensagem enviada - Com Sucesso
#Pré-requisto: Ter o amigo na lista de contatos

Dado que é desejado enviar uma nova mensagem
Quando é escolhido o amigo desejado
Então a mensagem é digitada
E enviada com sucesso


Cenário: Mensagem não enviada - Sem Sucesso
#Pré-requisitos: Ter o amigo na lista de contatos
#		 Não ter conexão com a internet

Dado que é desejado enviar uma nova mensagem
Quando é escolhido o amigo desejado
E a mensagem é digitada
Então há problema de conexão com a internet
E a mensagem não é enviada
